module.exports = {
  siteName: 'iamcollins',
  siteDescription: 'Llewellyn Collins personal website',
  siteUrl: 'https://iamcollins.dev/',
  plugins: [{
      use: '@gridsome/plugin-google-analytics',
      options: {
        id: 'UA-127934983-3'
      }
    },
    {
      use: '@gridsome/plugin-sitemap',
      options: {
        cacheTime: 600000
      }
    },
    {
      use: '@gridsome/source-contentful',
      options: {
        space: process.env.CONTENTFUL_SPACE_ID,
        accessToken: process.env.CONTENTFUL_TOKEN,
        host: 'cdn.contentful.com',
        environment: 'master',
        typeName: 'Contentful'
      }
    }
  ],
  css: {
    loaderOptions: {
      scss: {}
    }
  }
}