// Server API makes it possible to hook into various parts of Gridsome
// on server-side and add custom data to the GraphQL data layer.
// Learn more: https://gridsome.org/docs/server-api

// Changes here requires a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = function (api) {
    api.createPages(async ({ graphql, createPage }) => {
        const { data } = await graphql(`{
            allContentfulArticle {
                edges {
                  node {
                    id
                    title
                    slug
                    richContent
                  }
                }
              }
            }
        `)

        data.allContentfulArticle.edges.forEach(({ node }) => {
            createPage({
                path: `/article/${node.slug}`,
                component: './src/templates/Article.vue',
                context: {
                    id: node.id,
                    title: node.title,
                    richContent: node.richContent
                }
            })
        })
    })
}