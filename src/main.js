import DefaultLayout from '~/layouts/Default.vue'

import VueScrollTo from 'vue-scrollto'
import BootstrapVue from 'bootstrap-vue'
import './assets/styles/main.scss'

import {
  library
} from '@fortawesome/fontawesome-svg-core'

import {
  faTrophy,
  faCheck,
  faAngleRight
} from '@fortawesome/free-solid-svg-icons'
import {
  faGithub,
  faTwitter,
  faLinkedin,
  faGitlab,
  faInstagram,
  faStackOverflow
} from '@fortawesome/free-brands-svg-icons'
import {
  FontAwesomeIcon
} from '@fortawesome/vue-fontawesome'

library.add(
  faGithub,
  faTwitter,
  faGitlab,
  faInstagram,
  faLinkedin,
  faTrophy,
  faAngleRight,
  faStackOverflow
)

export default function ( Vue, {
  head,
} ) {
  Vue.use( VueScrollTo )
  Vue.use( BootstrapVue )
  Vue.component( 'Layout', DefaultLayout )
  Vue.component( 'font-awesome', FontAwesomeIcon )
  const title = 'iamcollins';
  const url = 'https://iamcollins.dev';
  const description = 'Llewellyn Collins personal website';
  head.meta.push(
    {
      rel: 'preconnect',
      href: url
    },
    {
      rel: 'dns-prefetch',
      href: url
    },
    {
      property: 'og:title',
      content: title
    },
    {
      property: 'og:description',
      content: description
    },
    {
      property: 'og:type',
      content: 'website'
    },
    {
      property: 'og:url',
      content: url
    },
    {
      property: 'og:site_name',
      content: title
    },
    {
      property: 'og:locale',
      content: 'en_GB'
    },
    {
      property: 'og:image',
      content: '/favicon.png'
    },
    {
      name: 'twitter:card',
      content: 'summary'
    },
    {
      name: 'twitter:creator',
      content: '@iamcollins'
    },
    {
      name: 'twitter:title',
      content: title
    },
    {
      name: 'twitter:url',
      content: url
    },
    {
      name: 'twitter:image',
      content: '/favicon.png'
    },
    {
      name: 'twitter:description',
      content: description
    },
    {
      name: `apple-mobile-web-app-capable`,
      content: `yes`
    },
    {
      name: `apple-mobile-web-app-status-bar-style`,
      content: `black`
    }
  )
}